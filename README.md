<img id="header" src="./resources/totsukuni_no_shoujo-1.jpg" align="right" width=65% />

<br>

> **tawa (tp)**: go, wander, towards
>
> **wile (tp)**: want, desire, need

1. [Software picks](#software)
2. [Installation](#installation)
3. [How to use](#howto)
4. [Credits](#credits)
5. [Gallery](#gallery)

<br clear="all">
<br>

This repo is very much geared towards serving myself first and foremost, so I recommend you look through everything if you fork/clone it, and change or remove parts you don't need. As of right now, it is also heavily in progress and may not work _quite_ as expected. Testing and feedback is always welcome. It will also be subject to semi-frequent breaking changes as my needs and picks in software change.

With this disclaimer out of the way, we humbly present:

## My software picks: <a id="software">

- **Linux distro**: <br>
  `Arch` on daily driver desktop. I've done my fair share of distro hopping, and I've come to the popular conclusion that nothing really tops Arch for a personal use desktop where you want a perfect balance of stability, customizabilty and community support. "Boo hoo, but an Arch update broke my bootloader" -- read arch news oomfie. <br>
  `Artix` on my Thinkpad. Much the same reasoning as Arch, but I wanted to play around in a systemd-less environment. Gotta say, I see the protestors' point. <br>
  `Ubuntu` on my home server, soon to be migrated to `Debian` or `Rocky`. I've had it with Ubuntu's "opinionated" decisions. <br>
- **Init**: `systemd` on Arch, and `openrc` on Artix
- **Shell**: [zsh](ttps://zsh.sourceforge.io/) + [starship](starship.rs) prompt and [znap](https://github.com/marlonrichert/zsh-snap) plugin manager
- **Text editor**: [nvim](https://github.com/neovim/neovim) + [lazy](https://github.com/folke/lazy.nvim) plugin manager
- **Terminal**: [alacritty](https://github.com/alacritty/alacritty/) + [tmux](https://github.com/tmux/tmux/)
- **AUR helper**: [paru](https://github.com/Morganamilo/paru)
- **File manager**: [nnn](https://github.com/jarun/nnn/) and/or [thunar](https://docs.xfce.org/xfce/thunar/start)
- **Browser**: [w3m](https://w3m.sourceforge.net/) for good things, and [waterfox](https://github.com/WaterfoxCo/Waterfox) for web 2.0+
- **Window manager**: [awesome](https://awesomewm.org/) + [picom](https://github.com/yshui/picom) on X11 and [niri](https://github.com/YaLTeR/niri) on Wayland
- **Status bar**: [polybar](https://github.com/polybar/polybar/) on X11 and [waybar](https://github.com/Alexays/Waybar) on Wayland
- **Launcher**: [rofi](https://github.com/davatorium/rofi)
- **Color theme**: [catppuccin](https://github.com/catppuccin/catppuccin), my beloved

## Installation <a id="installation">

Assuming you have a clean-slate installation of Ar[ch|tix|\*],

- If you intend to tweak and change these, which you probably (and hopefully) do, [fork](https://docs.github.com/en/get-started/quickstart/fork-a-repo) the repo first.

1. Clone the repo to `/opt/` or `~/.opt/`:

   ```sh
   git clone --depth 1 https://codeberg.org/snekmuffin/tawa-wile.git ~/.opt/tawa-wile/
   ```

   This location is actually optional, and needed for literally two aliases. So, up to you where to put it, really. Also, of course, change the url to your fork if you did fork it.

2. Run `./install.sh` and follow the installer. If you already have some configurations, pass `-o` to overwrite them. Be careful with that. You can also pass `-q` for blazing fast promptless installation. If you trust me.

   Running the script is also generally fine even if you already have your own stuff configured. By default, it will prompt before making any change, and will not overwrite anything.

3. Some manual labor:

- Launch tmux and run Lr-I. Leader is mapped to ^Space by default.
- If running Ubuntu, nvim might not source init.lua because Ubuntu is horse ass and nobody should use it. Easiest bet is to remove the package and build it from source.
- On a headless/lightweight install you might not want to install npm, along other things. This will cause errors, notably in nvim as Mason tries to install LSPs. For this, and similar issues, you can use `getreal ~/.config/nvim/lua/plugins/lsp.lua` to convert the symlink into a real file and edit it to remove some or all LSPs, without the change proliferating across your other machines. Once you push and pull that is.

4. Download wallpapers and lockscreens from [Google Drive](https://drive.google.com/drive/folders/1BT5KvXwI6yuY1ZV3on9DrGtwVCldP-6Y?usp=sharing), and put them at `~/.pretty`.

You can also always provide them yourself. Put wallpapers at `~/.pretty/wallpapers` and lockscreens at `~/.pretty/lockscreens`.

## How to use <a id="howto">

<img id="header" src="./resources/totsukuni_no_shoujo-2.jpg" align="left" width=60% />

Most importantly, look around! Open up files, steal whatever you like, change what you don't. I've tried my best to keep all code and configs clean and easy to understand. Mostly for my sake than yours. (nvim config is an exception to that, brrr)

All dotfiles will be installed as **symlinks** that point to a place inside the repo. After changing any of the configurations, you should be able to cd to the repo directory (using the handy little `tw` alias) and commit and push the changes (or just `twa` to commit all and move on). This way, every time you change the configs, the change is global across all your machines.

Note, of course, that when you add a separate new file to a config folder (say, a new plugin for nvim), it won't become a link. To propagate it to your repo, copy it inside the desired layer directory, and run `./install.sh -o` to overwrite the file with the new link.

<br clear="all">

To store machine-specific files, there are several local directories sourced by `.xprofile` with a DM or `.zlogin` without one:

- `~/.zshloc` for local zsh configuration. Good for aliases, functions, zsh plugins, etc. `qal` saves aliases here, for example.
- `~/.autorunloc` for scripts that should be run when the session starts. Good for startup scripts, launching programs, etc.
- `~/.environment` stores your environment variables you want sourced when the session starts. As long as you don't modify symlinked files, everything else will stay local.

I kind of implicitly assume that a stronger macine will run X11, and a machine more humble will run Wayland. So, the X11 config layer takes some frivolities with what it installs and runs, and is generally more resource-intensive. Wayland layer is pretty lightweight in comparison. Case in point: next paragraph.

### Notable features

- Dashboard! Comfy little thing that eats 250MB of ram cuz Alacritty lol. Shift+Meta+D to kill it if you're hurting. It is also configured for a 1440p screen, so it might look wonky on a different one. Configure `~/.scripts/x11-wm/dashboard/dashboard.sh` and `~/.config/awesome/startup/rules.lua` to change it.
- Keyboard-centric and made for Colemak. The _only_ time you really need to use a mouse is if you want to very precisely resize awesome's .windows. Also web browsing, cuz not even vimium-c can save some websites.
- All configs are as modular as I could make them: awesome and zsh are _very_ readable, and nvim is _mostly kinda_ readable.
- Repo is automatically pulled each time you log in. If there is an error, you'll get a notification.

**Aliases and shell functions**

- `t` to fuzzy cd to a directory and open a tmux session with the name of the directory
- `transon`, `transoff` and `transet` to configure Alacritty's background transparency
- `erc` to modify zsh configs
- `qal` to instantly add and source a local alias.
- `tw` to cd into `*opt/tawa-wile*`, and `twa` to push all dotfile changes.

See the full list on the [wiki](https://codeberg.org/aketawi/tawa-wile/wiki/Aliases).

**Shell keybinds**

- `alt+.` to instantly move up a dir
- `alt+-` to instantly move into the previous dir
- etc etc

See the full list on the [wiki](https://codeberg.org/aketawi/tawa-wile/wiki/Shell-Keybinds).

## Credits <a id="credits">

- [MeledoJames](https://github.com/MeledoJames/awesome-setup) for their amazing AwesomeWM setup I've used as the base.
- [adi1090x](https://github.com/adi1090x/rofi) for their beautiful rofi configs
- [joshmedeski](https://github.com/joshmedeski/t-smart-tmux-session-manager) for the idea for `t`
- [dotbot](https://github.com/anishathalye/dotbot/) for inspiring me to make an installer script.
- [oh-my-zsh](https://github.com/ohmyzsh/ohmyzsh/) for zsh config inspiration and a couple stolen aliases and functions.
- [Cascade](https://github.com/andreasgrafen/cascade/) for granting me the comfiest possible web browser experience.
- [LunarVim](https://github.com/LunarVim/Neovim-from-scratch) for general guidelines for setting up neovim plugins.
- Everyone whose images I've used for wallpapers and lockscreens. I am disheartened to admit I've never recorded where I got them from.

## Gallery <a id="gallery">

![x screenshot 1](./resources/screenshot_x-1.png)
![x screenshot 2](./resources/screenshot_x-2.png)
![x screenshot 3](./resources/screenshot_x-3.png)
![x screenshot 4](./resources/screenshot_x-4.png)

## Known issues:

- In nvim, Telescope won't search text inside symlinked files.
  Annoying when working with dotfiles.

## TODO:

- Make an uninstaller script that will completely revert and clean up an installer
- Root shell configs
- Wayland screenshots
- Reminders sourced from .zshloc that pop up each time you launch the shell
  Stuff like 'remember that popd and pushd exist'
- Start using [tridactyl](https://github.com/tridactyl/tridactyl) and make a catppuccin theme for it?
- start using [calcurse](https://github.com/lfos/calcurse)?
- yeet obsidian and live inside vim? [vimwiki](https://github.com/vimwiki/vimwiki), [mkdnflow](https://github.com/jakewvincent/mkdnflow.nvim#-installation-and-usage), [neorg](https://github.com/nvim-neorg/neorg)
- there's also [obsidian.nvim](https://github.com/epwalsh/obsidian.nvim)
- migrate the whole thing to makefile?
- migrate the whole thing to anisible?
- migrate the whole thing to chezmoi?

## Resources
https://gitea.exu.li/exu/configs
