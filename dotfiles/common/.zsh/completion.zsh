autoload -Uz compinit
compinit
setopt complete_in_word
setopt always_to_end

# fuzzy tab completion
zstyle ':completion:*' matcher-list '' \
  'm:{a-z\-}={A-Z\_}' \
  'r:[^[:alpha:]]||[[:alpha:]]=** r:|=* m:{a-z\-}={A-Z\_}' \
  'r:|?=** m:{a-z\-}={A-Z\_}'

# forgiving completion - up to 3 mistakes
zstyle ':completion:*' completer _complete _match _approximate
zstyle ':completion:*:match:*' original only # causes eza to freak out?
zstyle ':completion:*:approximate:*' max-errors 3 numeric

# formatting and messages
zstyle ':completion:*:descriptions' format '%B%d%b'
zstyle ':completion:*:messages' format '%d'
zstyle ':completion:*:warnings' format 'No matches for: %d'
zstyle ':completion:*:corrections' format '%B%d (errors: %e)%b'
zstyle ':completion:*' group-name ''

# only fill in unambiguous
zstyle ':autocomplete:*' insert-unambiguous yes

# Add simple colors to kill
zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#) ([0-9a-z-]#)*=01;34=0=01'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'

# man
zstyle ':completion:*:manuals' separate-sections true
zstyle ':completion:*:manuals.(^1*)' insert-sections true

# expand aliases
zstyle ':completion:*' completer _expand_alias _complete _ignored

# cache results for better performance
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.cache/zsh

### completion functions

_t_completion() {
  for s in $(tmux ls | cut -d: -f1); do
    COMPREPLY+=($s)
  done
}
complete -o default -F _t_completion t
