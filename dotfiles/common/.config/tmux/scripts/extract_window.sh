#!/bin/env bash
cur="$(tmux display-message -p '#S:#I')"
targ="$(tmux new -dPc "$(tmux display-message -p "#{pane_current_path}")")"
tmux swapw -s "$cur" -t "${targ}1"
tmux switchc -t "$targ"
