#!/bin/bash

# Handle splits in vim and nnn
# -h = horizontal
# -v = vertical

[[ $# -lt 1 ]] && exit

eval "$is_vim" && {
  [[ $1 == "-h" ]] && tmux send :vs Enter
  [[ $1 == "-v" ]] && tmux send :sp Enter
  exit 0
}

eval "$is_nnn" && {
  tmux split-window "$1" -c "$(lsof -w -c nnn | \grep cwd | tail -n 1 | awk '{print $9}')" nnn
  exit 0
}

tmux split-window "$1" -c "#{pane_current_path}"
exit 0
