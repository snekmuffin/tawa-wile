-- qol in terminal
function _G.set_terminal_keymaps()
  local opts = { noremap = true }
  vim.api.nvim_buf_set_keymap(0, 't', '<esc>', [[<C-o><C-n>]], opts)
  vim.api.nvim_buf_set_keymap(0, 't', '<C-n>', [[<C-o><C-n><C-W>h]], opts)
  vim.api.nvim_buf_set_keymap(0, 't', '<C-e>', [[<C-o><C-n><C-W>j]], opts)
  vim.api.nvim_buf_set_keymap(0, 't', '<C-i>', [[<C-o><C-n><C-W>k]], opts)
  vim.api.nvim_buf_set_keymap(0, 't', '<C-o>', [[<C-o><C-n><C-W>l]], opts)
end
