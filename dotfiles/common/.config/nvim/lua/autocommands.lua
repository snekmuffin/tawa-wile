vim.cmd [[
  augroup html
    autocmd!
    au FileType html,htmldjango inoremap <buffer> <C-b> <strong></strong><ESC>T>i
    au FileType html,htmldjango inoremap <buffer> <C-d> <div></div><ESC>T>i
    au FileType html,htmldjango inoremap <buffer> <C-s> <span></span><ESC>T>i
    au FileType html,htmldjango inoremap <buffer> <C-g> <p></p><ESC>T>i
    au FileType html,htmldjango inoremap <buffer> <C-a> <a href=""></a><ESC>F"i

    au FileType html,htmldjango vnoremap <buffer> <C-b> "ux<ESC>i<strong></strong><ESC>F>"upi
    au FileType html,htmldjango vnoremap <buffer> <C-d> "ux<ESC>i<div></div><ESC>F>"upi
    au FileType html,htmldjango vnoremap <buffer> <C-s> "ux<ESC>i<span></span><ESC>F>"upi
    au FileType html,htmldjango vnoremap <buffer> <C-g> "ux<ESC>i<p></p><ESC>F>"upi
    au FileType html,htmldjango vnoremap <buffer> <C-a> "ux<ESC>i<a href=""></a><ESC>F>"upF"i
  augroup END

  augroup zsh_as_bash
    autocmd!
    au BufNewFile,BufRead *.*sh :set ft=bash
  augroup END

  augroup highlight_yank
    autocmd!
    au TextYankPost * silent! lua vim.highlight.on_yank { higroup='IncSearch', timeout=100 }
  augroup END

  augroup _general_settings
    autocmd!
    autocmd FileType qf,help,man,lspinfo nnoremap <silent> <buffer> q :close<CR>
    " autocmd TextYankPost * silent!lua require('vim.highlight').on_yank({higroup = 'Search', timeout = 200})
    " autocmd BufWinEnter * :set formatoptions-=cro
    autocmd FileType qf set nobuflisted
  augroup end

  augroup _git
    autocmd!
    autocmd FileType gitcommit setlocal wrap
    autocmd FileType gitcommit setlocal spell
  augroup end

  augroup _markdown
    autocmd!
    au FileType text,markdown inoremap <buffer> <C-b> ****<ESC>F*i
    au FileType text,markdown inoremap <buffer> <C-e> **<ESC>T*i
    au FileType text,markdown inoremap <buffer> <C-k> []()<ESC>T[i

    au FileType text,markdown vnoremap <buffer> <C-b> "ux<ESC>i****<ESC>2F*"upi
    au FileType text,markdown vnoremap <buffer> <C-e> "ux<ESC>i**<ESC>F*"upi
    au FileType text,markdown vnoremap <buffer> <C-k> "ux<ESC>i[]()<ESC>F["upi

    au FileType text,markdown setlocal wrap
    au FileType text,markdown setlocal spell
    au FileType text,markdown setlocal nonumber
    au FileType markdown set autowriteall
    au BufLeave *.md silent! wall
  augroup end

  augroup _auto_resize
    autocmd!
    autocmd VimResized * tabdo wincmd =
  augroup end

  augroup _alpha
    autocmd!
    autocmd User AlphaReady set showtabline=0 | autocmd BufUnload <buffer> set showtabline=2
  augroup end

  aunmenu PopUp.How-to\ disable\ mouse
  aunmenu PopUp.-1-
]]
