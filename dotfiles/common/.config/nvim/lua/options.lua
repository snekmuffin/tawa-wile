-- Use `:h <option>` to figure out the meaning if needed
vim.opt.backupdir = "/tmp/nvim/backups/"
vim.opt.backup = true             -- yes backups
vim.opt.writebackup = true        -- yes temporary backups
vim.opt.undofile = true           -- enable persistent undo
vim.opt.fileencoding = "utf-8"    -- the encoding written to a file
vim.opt.clipboard = "unnamedplus" -- use system clipboard
vim.opt.completeopt = {           -- options for completion plugin
  "menu", "menuone", "preview", "noinsert"
}
vim.opt.shortmess:append "c" -- suppress completion messages
vim.opt.mouse = "a"          -- need mouse support to hijack mouse wheel from tmux

-- Tabs
vim.opt.expandtab = true -- convert tabs to spaces
vim.opt.shiftwidth = 2   -- the number of spaces inserted for each indentation
vim.opt.tabstop = 2      -- insert 2 spaces for a tab
vim.opt.softtabstop = 2  -- number of spacesin tab when editing
vim.opt.showtabline = 2  -- always show tabs

-- UI
vim.opt.number = true         -- set numbered lines
vim.opt.relativenumber = true -- set relative numbered lines
vim.opt.numberwidth = 4       -- set number column width

vim.opt.cursorline = true     -- highlight the current line
vim.opt.cmdheight = 0         -- space in the command line
vim.opt.conceallevel = 1      -- mostly to hide stuff in markdown files
vim.opt.showmode = false      -- remove mode alert because it will be handled by lualine

vim.opt.signcolumn = "yes"    -- always show the sign column, otherwise it would shift the text each time
vim.opt.wrap = false          -- disable word wrapping by default

vim.opt.splitbelow = true     -- force horizontal splits to go below current window
vim.opt.splitright = true     -- force vertical splits to go to the right
vim.opt.colorcolumn = "89"    -- show column line at max line length (following Black)

-- vim.opt.laststatus = 0      -- hide status line, which will be handled by Lualine

-- Folds, optimized for ufo
vim.o.foldcolumn = '0'
vim.o.foldlevel = 99
vim.o.foldlevelstart = 99
vim.o.foldenable = true

-- Search
vim.opt.incsearch = true  -- search as characters are entered
vim.opt.hlsearch = true   -- highlight all matches on previous search pattern
vim.opt.ignorecase = true -- ignore case in search patterns
vim.opt.smartcase = true  -- but make it case sensitive if an uppercase is entered

-- Editing
vim.opt.smartindent = true       -- auto indent on line breaks
vim.opt.scrolloff = 12           -- minimal number of screen lines to keep above and below the cursor
vim.opt.sidescrolloff = 8        -- same thing but for horizontals

vim.opt.timeoutlen = 1000        -- time to wait for a mapped sequence to complete (ms)
vim.opt.updatetime = 300         -- faster completion (4000ms default)

vim.opt.virtualedit = "block"    -- better ^v

vim.cmd "set linebreak"          -- wrap lines at whitespaces only
vim.cmd "set whichwrap+=<,>,[,]" -- wrap cursor for <> and [] but not directionals
vim.cmd "set iskeyword-=_"       -- treat underscores as word breaks
