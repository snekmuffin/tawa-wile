vim.cmd [[highlight Dash guifg=#eed49f gui=bold]]
vim.cmd [[highlight Headline guibg=#363a4f gui=bold]]
vim.cmd [[highlight @text.emphasis.markdown_inline  guifg=#fab387]]
vim.cmd [[highlight @text.strong.markdown_inline guifg=#f9e2af gui=bold]]

return {
  {
    "ellisonleao/glow.nvim",
    opts = {
      width_ratio = 1,
      height_ratio = 1,
      border = "none",
      style = "~/.config/glow/catppuccin.json",
      width = 120
    },
    cmd = "Glow"
  },
  {
    "lukas-reineke/headlines.nvim",
    dependencies = "nvim-treesitter/nvim-treesitter",
    opts = {
      markdown = {
        fat_headline_upper_string = "▃",
        fat_headline_lower_string = "▔",
        fat_headlines = true,
        dash_string = "—",
        codeblock_highlight = "CodeBlock",
      },
    }
  },
}
