-- indent line visualization
return {
  "lukas-reineke/indent-blankline.nvim",
  name = "indent-blankline",
  main = "ibl",
  opts = {
    indent = {
      char = "▏",
      highlight = { "VertSplit" },
      smart_indent_cap = true,
      priority = 2,
    },
    scope = {
      enabled = true,
      show_start = true,
      show_end = false,
      injected_languages = false,
      highlight = { "Function", "Label" },
      priority = 500,
    }
  }
}
