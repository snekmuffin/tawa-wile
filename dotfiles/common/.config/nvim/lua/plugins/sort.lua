-- sort lines in visual
-- binds in whichkey
return {
  'sQVe/sort.nvim',
  config = function()
    require("sort").setup()
  end
}
