-- pseudo LSP for formatting files and providing linting
return {
  "jose-elias-alvarez/null-ls.nvim",
  dependencies = { "nvim-lua/plenary.nvim" },
  config = function()
    local null_ls = require("null-ls")
    null_ls.setup({
      sources = {
        null_ls.builtins.diagnostics.ansiblelint,
        null_ls.builtins.diagnostics.shellcheck,
        null_ls.builtins.formatting.black,           -- python
        null_ls.builtins.formatting.prettierd.with({ -- catchall
          extra_filetypes = { "toml", "html", "htmldjango" },
          disabled_filetypes = { "python", "lua" }
        }),
        null_ls.builtins.formatting.shfmt.with({ -- shell
          extra_args = { "-i", "2", "-ci" },
          filetypes = { "sh", "zsh", "bash" }
        }),
      },
    })
  end,
}
