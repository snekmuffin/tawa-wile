-- extra functions for ^A and ^X
return {
  "monaqa/dial.nvim",
  config = function()
    local augend = require("dial.augend")
    require("dial.config").augends:register_group {
      default = {                      -- default augends used when no group name is specified
        augend.integer.alias.decimal,  -- nonnegative decimal number (0, 1, 2, 3, ...)
        augend.integer.alias.hex,      -- nonnegative hex number  (0x01, 0x1a1f, etc.)
        augend.date.alias["%Y/%m/%d"], -- date (2022/02/19, etc.)
        augend.constant.alias.bool,    -- true/false
        augend.semver.alias.semver,    -- sem versions

        augend.constant.new {
          elements = { "True", "False" }, -- capitalized
          word = false,
          cyclic = true,
        },

        augend.constant.new {
          elements = { "and", "or" },
          word = true, -- if false, "sand" is incremented into "sor", "doctor" into "doctand", etc.
          cyclic = true,
        },

        augend.constant.new {
          elements = { "&&", "||" },
          word = false,
          cyclic = true,
        },

        augend.constant.new {
          elements = { "&", "|" },
          word = false,
          cyclic = true,
        },
      },

      visual = {
        augend.integer.alias.decimal,
        augend.integer.alias.hex,
        augend.constant.alias.alpha,
        augend.constant.alias.Alpha,
      },
    }

    vim.keymap.set("n", "<C-a>", require("dial.map").inc_normal(), { noremap = true })
    vim.keymap.set("n", "<C-x>", require("dial.map").dec_normal(), { noremap = true })
    vim.keymap.set("v", "<C-a>", require("dial.map").inc_visual("visual"), { noremap = true })
    vim.keymap.set("v", "<C-x>", require("dial.map").dec_visual("visual"), { noremap = true })
  end
}
