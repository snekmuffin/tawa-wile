-- insert a second pair for quotes and brackets
return {
  "windwp/nvim-autopairs",
  name = "nvim-autopairs",
  event = "InsertEnter",
  config = function()
    local pairs = require("nvim-autopairs")
    pairs.setup({
      check_ts = true,
      ts_config = {
        lua = { "string", "source" },
        javascript = { "string", "template_string" },
        java = false,
      },
      disable_filetype = { "TelescopePrompt", "spectre_panel" },
      fast_wrap = {
        map = "<M-r>",
        chars = { "{", "[", "(", '"', "'", "<" },
        pattern = string.gsub([[ [%'%"%)%>%]%)%}%,] ]], "%s+", ""),
        offset = 0, -- Offset from pattern match
        end_key = "$",
        keys = "arstdhneiowfuyxcqplzxcvkm",
        check_comma = true,
        highlight = "PmenuSel",
        highlight_grey = "LineNr",
      },
    })
  end,
}
