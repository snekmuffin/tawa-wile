-- pop out UI into floating messages
return {
  "folke/noice.nvim",
  lazy = true,
  event = "VeryLazy",
  dependencies = {
    "MunifTanjim/nui.nvim",
    "rcarriga/nvim-notify",
    "VonHeikemen/lsp-zero.nvim", -- make sure it loads after the LSP and
  },                             -- overrides hover and signature windows

  config = function()
    require("noice").setup({
      lsp = {
        override = { -- override markdown rendering so that cmp and other plugins use Treesitter
          ["vim.lsp.util.convert_input_to_markdown_lines"] = true,
          ["vim.lsp.util.stylize_markdown"] = true,
          ["cmp.entry.get_documentation"] = true,
        },
        progress = { enabled = true }, -- turn on for diagnostics
        hover = { enabled = true },
        signature = { enabled = true, },

      },
      presets = {
        command_palette = true,       -- position the cmdline and popupmenu together
        long_message_to_split = true, -- long messages will be sent to a split
        inc_rename = false,           -- enables an input dialog for inc-rename.nvim
        lsp_doc_border = true,        -- add a border to hover docs and signature help
      },
      routes = {
        {
          filter = { -- turn off :w messages
            event = "msg_show",
            kind = "",
            find = "written",
          },
          opts = { skip = true },
        },
      },
    })
  end
}
