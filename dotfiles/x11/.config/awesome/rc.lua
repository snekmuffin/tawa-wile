--[[
  huge thanks to MeledoJames, as this setup is largely
  adapted from their work:
  https://github.com/MeledoJames/awesome-setup
--]]
pcall(require, "luarocks.loader")

User_prefs = {

  -- applications
  terminal = "alacritty",
  editor = "alacritty -e" .. "nvim",
  web = "waterfox",
  files = "alacritty -e" .. "nnn",

  -- user profile
  username = os.getenv("USER"):gsub("^%l", string.upper),
  userdesc = "@hognose", -- your hostname or something nice
  home = os.getenv("HOME"),

  -- controls
  modkey = "Mod4", -- meta key
}

-- prevent naughty from loading to give way to dunst
local _dbus = dbus; dbus = nil
local naughty = require("naughty")
dbus = _dbus

-- apply visuals
require("theme")

-- apply behavior
require("config")

-- run startup scripts
require("startup")
