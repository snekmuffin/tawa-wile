-- deps
local awful = require("awful")
local hotkeys_popup = require("awful.hotkeys_popup")

-- modkeys
local modkey = "Mod4"
local shift = "Shift"
local ctrl = "Control"
local alt = "Mod1"

-- preferences
local terminal = "alacritty"
local terminal_class = "Alacritty"


-- general keys
-- keep in mind that they these imply the Colemak layout.
-- switch the keys around if you use a different one.
awful.keyboard.append_global_keybindings({

  awful.key({ modkey }, "z", function()
    awful.spawn.with_shell("polybar-msg cmd hide")
  end, { description = "hide Polybar", group = "client" }),

  awful.key({ shift, modkey }, "z", function()
    awful.spawn.with_shell("polybar-msg cmd show")
  end, { description = "show Polybar", group = "client" }),

  awful.key({ alt, modkey }, "z", function()
    awful.spawn.with_shell("sh ~/.scripts/x11-wm/reset-polybar.sh")
  end, { description = "restart Polybar", group = "client" }),

  awful.key({ modkey }, "x", function()
    awful.spawn.with_shell("dunstctl close-all")
  end, { description = "dismiss notifications", group = "client" }),

  awful.key({ shift, modkey }, "x", function()
    awful.spawn.with_shell("dunstctl history-pop")
  end, { description = "show last notification", group = "client" }),

  awful.key({ modkey }, "b", function()
    awful.spawn.with_shell("~/.opt/boomer/boomer")
  end, { description = "open boomer", group = "client" }),

  awful.key({ modkey }, "t", function()
    awful.spawn.with_shell("picom-trans -c +10")
  end, { description = "increase opacity of current window", group = "client" }),
  awful.key({ modkey, shift }, "t", function()
    awful.spawn.with_shell("picom-trans -c -10")
  end, { description = "decrease opacity of current window", group = "client" }),

  -- client focus --------------------------------------------------------------------
  awful.key({ modkey }, "n", --h
    function()
      awful.client.focus.byidx(-1)
    end, { description = "focus previous", group = "focus" }),

  awful.key({ modkey }, "o", --l
    function()
      awful.client.focus.byidx(1)
    end, { description = "focus next", group = "focus" }),

  awful.key({ modkey }, "Tab",
    function()
      awful.client.focus.byidx(1)
    end, { description = "focus next", group = "focus" }),

  awful.key({ modkey }, "e", --j
    function()
      awful.screen.focus_relative(1)
    end, { description = "focus next screen", group = "focus" }),

  awful.key({ modkey }, "i", --k
    function()
      awful.screen.focus_relative(-1)
    end, { description = "focus previous screen", group = "focus" }),

  awful.key({ modkey }, "l", --u
    awful.client.urgent.jumpto,
    { description = "jump to urgent client", group = "focus" }),

  awful.key({ modkey, ctrl }, "k",
    function()
      local c = awful.client.restore()
      if c then
        c:activate({ raise = true, context = "key.unminimize" })
      end
    end, { description = "restore minimized", group = "focus" }),

  -- client layout --------------------------------------------------------------------
  awful.key({ modkey, shift }, "n", function()
    awful.client.swap.byidx(-1)
  end, { description = "swap with previous client", group = "layout" }),

  awful.key({ modkey, shift }, "o", function()
    awful.client.swap.byidx(1)
  end, { description = "swap with next client", group = "layout" }),

  awful.key({ modkey, alt }, "n", function()
    awful.tag.incmwfact(-0.05)
  end, { description = "decrease master width factor", group = "layout" }),

  awful.key({ modkey, alt }, "o", function()
    awful.tag.incmwfact(0.05)
  end, { description = "increase master width factor", group = "layout" }),

  awful.key({ modkey, alt }, "e", function()
    awful.tag.incnmaster(1, nil, true)
  end, { description = "decrease number of master clients", group = "layout" }),
  awful.key({ modkey, alt }, "i", function()
    awful.tag.incnmaster(-1, nil, true)
  end, { description = "increase number of master clients", group = "layout" }),
  awful.key({ modkey, alt, ctrl }, "e", function()
    awful.tag.incncol(-1, nil, true)
  end, { description = "decrease the number of columns", group = "layout" }),
  awful.key({ modkey, alt, ctrl }, "i", function()
    awful.tag.incncol(1, nil, true)
  end, { description = "increase the number of columns", group = "layout" }),

  awful.key({ modkey }, ",", function()
    awful.layout.inc(-1)
  end, { description = "select previous layout", group = "layout" }),

  awful.key({ modkey }, ".", function()
    awful.layout.inc(1)
  end, { description = "select next layout", group = "layout" }),

  -- tags --------------------------------------------------------------------
  awful.key({ modkey }, "Escape",
    awful.tag.history.restore,
    { description = "go back", group = "tag" }),

  awful.key({
    modifiers = { modkey },
    keygroup = "numrow",
    description = "view tag",
    group = "tag",
    on_press = function(index)
      local screen = awful.screen.focused()
      local tag = screen.tags[index]
      if tag then
        tag:view_only()
      end
    end,
  }),

  awful.key({
    modifiers = { modkey, shift },
    keygroup = "numrow",
    description = "move focused client to tag",
    group = "tag",
    on_press = function(index)
      if client.focus then
        local tag = client.focus.screen.tags[index]
        if tag then
          client.focus:move_to_tag(tag)
        end
      end
    end,
  }),

  -- media --------------------------------------------------------------------
  awful.key({ modkey }, "F6", function()
    awful.util.spawn("playerctl play-pause", false)
  end, { description = "play or pause media", group = "media" }),

  awful.key({ modkey }, "F7", function()
    awful.util.spawn("playerctl previous", false)
  end, { description = "previous track", group = "media" }),

  awful.key({ modkey }, "F8", function()
    awful.util.spawn("playerctl next", false)
  end, { description = "next track", group = "media" }),

  -- change the number after `-i` or `-d` to change the volume step
  awful.key({ modkey, shift }, "Up", function()
    awful.util.spawn("pamixer -i 1", false)
  end, { description = "volume up", group = "media" }),

  awful.key({ modkey, shift }, "Down", function()
    awful.util.spawn("pamixer -d 1", false)
  end, { description = "volume down", group = "media" }),

  -- launchers --------------------------------------------------------------------
  awful.key({ modkey }, "a", function()
    awful.spawn.with_shell("sh ~/.config/rofi/apps.sh")
  end, { description = "run rofi apps (^Tab for shell run)", group = "launcher" }),

  awful.key({ modkey }, "w", function()
    awful.spawn.with_shell("sh ~/.config/rofi/windows.sh")
  end, { description = "run rofi windows", group = "launcher" }),

  awful.key({ modkey }, "r", function()
    awful.spawn.with_shell("sh ~/.config/rofi/calc.sh")
  end, { description = "run rofi calc", group = "launcher" }),

  awful.key({ modkey }, "s", function()
    awful.spawn.with_shell("sh ~/.config/rofi/clipboard.sh")
  end, { description = "run rofi clipboard", group = "launcher" }),

  awful.key({ modkey }, "f", function()
    local c = client.focus
    -- if c and (c.class == terminal_class or c.class == 'filemanager') then
    if c and (c.class == 'filemanager') then
      c:kill()
    else
      awful.spawn.with_shell("alacritty --class 'filemanager' -e tmux new -ADs nnn nnn")
    end
  end, { description = "run file manager", group = "launcher" }),

  awful.key({ modkey }, "'", function()
    awful.spawn.with_shell("obsidian")
  end, { description = "run obsidian", group = "launcher" }),

  awful.key({ modkey }, "`", function()
    awful.spawn.with_shell("sh ~/.config/rofi/power.sh")
  end, { description = "power options", group = "launcher" }),

  awful.key({ modkey }, "Return", function()
    local c = client.focus
    if c and (c.class == terminal_class or c.class == 'biglacritty') then
      c:kill()
    else
      awful.spawn.with_shell("alacritty --class 'biglacritty' -e tmux new -ADs tawi-main", {
        fullscreen = true,
      })
    end
  end, { description = "open or close a fullscreen terminal", group = "misc" }),

  awful.key({ shift, modkey }, "Return", function()
    awful.spawn(terminal)
  end, { description = "open a terminal indiscriminately", group = "misc" }),

  -- misc --------------------------------------------------------------------
  awful.key({ modkey }, "F1",
    function()
      local screen = awful.screen.focused()
      hotkeys_popup.widget.new({
        modifiers_fg = "#a5adcb",
        width = 1440,
        font = "FiraCode Nerd Font 12",
        description_font = "FiraCode Nerd Font 10"
      }):show_help(nil, screen)
    end,
    { description = "show this popup", group = "misc" }),

  awful.key({ modkey, ctrl }, "r",
    awesome.restart,
    { description = "restart Awesome", group = "misc" }),

  awful.key({ modkey, ctrl }, "q",
    awesome.quit,
    { description = "quit Awesome", group = "misc" }),

  awful.key({ modkey, alt }, "q", function()
    awful.spawn.with_shell("sh ~/.scripts/x11-wm/superkill.sh")
  end, { description = "superkill a client", group = "client" }),

  awful.key({ modkey }, "c", function()
    awful.spawn.with_shell("sh ~/.scripts/x11-autorun/background.sh")
  end, { description = "change background", group = "client" }),

  awful.key({}, "Print", function()
    awful.spawn.with_shell("flameshot gui")
  end, { description = "take a screenshot", group = "misc" }),

  awful.key({ modkey }, "d", function()
    awful.spawn.with_shell("sh ~/.scripts/x11-wm/dashboard/init-dashboard.sh")
  end, { description = "Spawn dashboard", group = "misc" }),

  awful.key({ modkey, shift }, "d", function()
    awful.spawn.with_shell("sh ~/.scripts/x11-wm/dashboard/kill-dashboard.sh")
  end, { description = "Kill dashboard", group = "misc" }),

  awful.key({ modkey }, "v", function()
    awful.spawn.with_shell(
      "sudo usb_resetter --reset-device -d $(sudo usb_resetter --list | grep Kensington | awk '{print $3}')")
  end, { description = "Reset the little shit", group = "misc" }),
})

-- mouse --------------------------------------------------------------------
client.connect_signal("request::default_mousebindings", function()
  awful.mouse.append_client_mousebindings({

    awful.button({}, 1, function(c)
      c:activate({ context = "mouse_click" })
    end),

    awful.button({ modkey }, 1, function(c)
      c:activate({ context = "mouse_click", action = "mouse_move" })
    end),

    awful.button({ modkey }, 3, function(c)
      c:activate({ context = "mouse_click", action = "mouse_resize" })
    end),
  })
end)

-- client specific bindings
client.connect_signal("request::default_keybindings", function()
  awful.keyboard.append_client_keybindings({

    awful.key({ modkey }, "q", function(c)
      c:kill()
    end, { description = "close", group = "client" }),

    awful.key({ modkey }, "k", function(c)
      c.minimized = true
    end, { description = "minimize", group = "client" }),

    awful.key({ modkey }, "p", function(c)
      c.fullscreen = not c.fullscreen
      c:raise()
    end, { description = "toggle fullscreen", group = "client" }),

    awful.key({ modkey, ctrl }, "p", function(c)
      awful.client.floating.toggle(c)
      c.width = 1000
      c.height = 550
      awful.placement.centered(c)
    end, { description = "toggle floating", group = "client" }),

    awful.key({ modkey }, "g", function(c)
      c.ontop = not c.ontop
    end, { description = "toggle keep on top", group = "client" }),

    awful.key({ modkey }, "m", function(c)
      c.maximized = not c.maximized
      c:raise()
    end, { description = "(un)maximize", group = "client" }),

    awful.key({ modkey, ctrl }, "m", function(c)
      c.maximized_vertical = not c.maximized_vertical
      c:raise()
    end, { description = "(un)maximize vertically", group = "client" }),

    awful.key({ modkey, shift }, "m", function(c)
      c.maximized_horizontal = not c.maximized_horizontal
      c:raise()
    end, { description = "(un)maximize horizontally", group = "client" }),

    awful.key({ modkey, shift }, "n", function(c)
      c:swap(awful.client.getmaster())
    end, { description = "move to master", group = "client" }),

    awful.key({ modkey, shift }, "i", function(c)
      c:move_to_screen()
    end, { description = "move to next screen", group = "client" }),

    awful.key({ modkey, ctrl }, "t", function(c)
      awful.spawn.with_shell("picom-trans -c -o 60")
      c.floating = true
      c.width = 500
      c.height = 750
      c.x = 2042
      c.y = 12
      -- c.focusable = not c.focusable
      c.ontop = not c.ontop
    end, { description = "Pop out client into overlay", group = "client" }),
    -- useful for twitch chat or discord for example
  })
end)
