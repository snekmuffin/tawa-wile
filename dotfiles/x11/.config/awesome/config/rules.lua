-- deps
local awful = require("awful")
local beautiful = require("beautiful")
local ruled = require("ruled")

ruled.client.connect_signal("request::rules", function()
  -- Global
  ruled.client.append_rule({
    id = "global",
    rule = {},
    properties = {
      focus = awful.client.focus.filter,
      raise = true,
      size_hints_honor = false,
      screen = awful.screen.preferred,
      placement = awful.placement.no_overlap + awful.placement.no_offscreen,
    },
  })

  -- Floating clients
  ruled.client.append_rule({
    id = "floating",
    rule_any = {
      instance = { "feh", "imv", "pavucontrol", "exe" },
      class = { "Arandr", "Blueman-manager", "Dragon-drop", "exe", "steam_proton", "wine" },
      name = {},
      role = { "pop-up" },
    },
    properties = { floating = true, placement = awful.placement.centered },
  })

  -- Borders
  ruled.client.append_rule({
    id = "borders",
    properties = {
      border_width = 2,
      border_color = beautiful.border_normal,
    },
  })

  -- Center
  ruled.client.append_rule({
    id = "center_placement",
    rule_any = {
      type = { "dialog" },
      class = {},
      instance = { "pavucontrol" },
      role = { "GtkFileChooserDialog" },
    },
    properties = { placement = awful.placement.centered },
  })

  -- Titlebars
  ruled.client.append_rule({
    id = "titlebars",
    rule_any = {
      type = { "dialog", "splash" },
      name = { "file_progress" },
    },
    properties = { titlebars_enabled = false },
  })

  -- per client ------------------------------------------------

  ruled.client.append_rule({
    rule_any = { class = { "firefox", "waterfox" } },
    except_any = {
      name = { "^Save As", "^ChatReplay" }, -- make sure download popups and such
      type = { "dialog", "splash" },        -- appear on the active screen
    },
    properties = { screen = 1, tag = "3", maximized = false },
  })

  ruled.client.append_rule({
    rule_any = { class = { "biglacritty" } },
    properties = { fullscreen = true },
  })

  ruled.client.append_rule({
    rule_any = { class = { "constellation_bgr" } },
    properties = { fullscreen = true, focusable = false, tag = "1", ontop = false, border_width = 0 },
  })

  ruled.client.append_rule({
    rule = { instance = "polybar" },
    properties = {
      border_width = 0,
      focusable = false,
      ontop = true
    },
  })

  ruled.client.append_rule({
    rule_any = { class = { "obsidian" } },
    properties = { screen = 1, tag = "2" },
  })

  ruled.client.append_rule({
    rule_any = { class = { "steam", "sdl", "Steam" } },
    properties = { screen = 1, tag = "4" },
  })

  ruled.client.append_rule({
    rule_any = { class = { "gamescope" } },
    properties = { screen = 1, tag = "4", fullscreen = true },
  })

  ruled.client.append_rule({
    rule_any = { class = { "cfx concert grand x64.exe", "easyeffects" } },
    properties = { screen = 1, tag = "5", floating = true },
  })

  ruled.client.append_rule({
    rule_any = { class = { "Feishin", "feishin" } },
    properties = { screen = 1, tag = "5" },
  })

  ruled.client.append_rule({
    rule_any = { class = { "TogglDesktop", "Toggl Desktop" } },
    properties = {
      screen = 2,
      tag = "1",
      floating = true,
      ontop = true,
      geometry = {
        x = 3514,
        y = 460,
        height = 610,
        width = 400,
      },
    },
  })

  ruled.client.append_rule({
    rule_any = { class = { "mpv" } },
    properties = { ontop = false },
  })

  ruled.client.append_rule({
    properties = { maximized = false },
  })
end)
