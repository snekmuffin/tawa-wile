local ruled = require("ruled")

ruled.client.connect_signal("request::rules", function()
  --------------------------------------------
	-- dashboard rules
  --------------------------------------------
	ruled.client.append_rule({
		rule_any = { class = { "startup-btop" } },
		properties = {
			screen = 1,
			tag = "1",
			floating = true,
			width = 1305,
			height = 720,
			x = 50,
			y = 53,
		},
	})

	ruled.client.append_rule({
		rule_any = { class = { "startup-cal" } },
		properties = {
			screen = 1,
			tag = "1",
			floating = true,
			width = 1090,
			height = 720,
			x = 1410,
			y = 53,
		},
	})

	ruled.client.append_rule({
		rule_any = { class = { "startup-rss" } },
		properties = {
			screen = 1,
			tag = "1",
			floating = true,
			width = 805,
			height = 558,
			x = 50,
			y = 823,
		},
	})

	ruled.client.append_rule({
		rule_any = { class = { "startup-fetch" } },
		properties = {
			screen = 1,
			tag = "1",
			floating = true,
			width = 455,
			height = 180,
			x = 905,
			y = 823,
		},
	})

	ruled.client.append_rule({
		rule_any = { class = { "startup-jrnl" } },
		properties = {
			screen = 1,
			tag = "1",
			floating = true,
			width = 1090,
			height = 564,
			x = 1410,
			y = 823,
		},
	})

	ruled.client.append_rule({
		rule_any = { class = { "startup-quote" } },
		properties = {
			screen = 1,
			tag = "1",
			floating = true,
			width = 455,
			height = 334,
			x = 905,
			y = 1053,
		},
	})
end)
