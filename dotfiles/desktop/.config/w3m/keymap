# Adapted from https://github.com/Badacadabra/Vimpressionist/blob/master/w3m/keymap
# Much thanks to them!
#
# -----------------------------
# Unmap default keys
# -----------------------------

keymap UP NULL
keymap RIGHT NULL
keymap DOWN NULL
keymap LEFT NULL
keymap DEL NULL
keymap SPC NULL
keymap TAB NULL
keymap RET NULL

keymap a NULL
keymap b NULL
keymap c NULL
keymap d NULL
keymap f NULL
keymap g NULL
keymap h NULL
keymap i NULL
keymap j NULL
keymap k NULL
keymap l NULL
keymap m NULL
keymap n NULL
keymap o NULL
keymap q NULL
keymap r NULL
keymap s NULL
keymap t NULL
keymap u NULL
keymap v NULL
keymap w NULL
keymap y NULL
keymap z NULL

keymap B NULL
keymap D NULL
keymap E NULL
keymap F NULL
keymap G NULL
keymap H NULL
keymap I NULL
keymap J NULL
keymap K NULL
keymap L NULL
keymap M NULL
keymap N NULL
keymap 0 NULL
keymap Q NULL
keymap R NULL
keymap S NULL
keymap T NULL
keymap U NULL
keymap V NULL
keymap W NULL
keymap Z NULL

keymap gg NULL
keymap gf NULL
keymap gt NULL
keymap gT NULL

keymap ZZ NULL
keymap ZQ NULL

keymap = NULL
keymap - NULL
keymap + NULL
keymap < NULL
keymap > NULL
keymap . NULL
keymap : NULL
keymap , NULL
keymap ^ NULL
keymap ( NULL
keymap ) NULL
keymap } NULL
keymap { NULL
keymap [ NULL
keymap ] NULL
keymap | NULL
keymap $ NULL
keymap @ NULL
keymap / NULL
keymap ! NULL
keymap ? NULL
keymap ";" NULL
keymap "#" NULL
keymap \" NULL
keymap \\ NULL

keymap C-a NULL
keymap C-b NULL
keymap C-d NULL
keymap C-e NULL
keymap C-f NULL
keymap C-g NULL
keymap C-h NULL
keymap C-j NULL
keymap C-k NULL
keymap C-l NULL
keymap C-m NULL
keymap C-n NULL
keymap C-o NULL
keymap C-p NULL
keymap C-q NULL
keymap C-r NULL
keymap C-s NULL
keymap C-t NULL
keymap C-u NULL
keymap C-v NULL
keymap C-w NULL
keymap C-y NULL
keymap C-z NULL
keymap C-wL NULL
keymap C-wH NULL
keymap C-] NULL
keymap C-@ NULL
keymap gC-g NULL

keymap ESC-TAB NULL
keymap ESC-a NULL
keymap ESC-b NULL
keymap ESC-c NULL
keymap ESC-e NULL
keymap ESC-g NULL
keymap ESC-k NULL
keymap ESC-l NULL
keymap ESC-m NULL
keymap ESC-n NULL
keymap ESC-o NULL
keymap ESC-p NULL
keymap ESC-s NULL
keymap ESC-t NULL
keymap ESC-u NULL
keymap ESC-v NULL
keymap ESC-w NULL
keymap ESC-I NULL
keymap ESC-M NULL
keymap ESC-W NULL
keymap ESC-: NULL
keymap ESC-< NULL
keymap ESC-> NULL
keymap ESC-C-j NULL
keymap ESC-C-m NULL
keymap ESC-C-j NULL

keymap ^[[1~ NULL
keymap ^[[2~ NULL
keymap ^[[4~ NULL
keymap ^[[5~ NULL
keymap ^[[6~ NULL
keymap ^[[28~ NULL
keymap ^[[E NULL
keymap ^[[L NULL
keymap ^[[Z NULL

# -----------------------------
# In-page Navigation
# -----------------------------

# Move to next page
keymap C-f NEXT_PAGE
keymap C-d NEXT_PAGE

# Move to previous page
keymap C-b PREV_PAGE
keymap C-u PREV_PAGE

# Move cursor
keymap o MOVE_RIGHT1
keymap n MOVE_LEFT1
keymap e MOVE_DOWN1
keymap i MOVE_UP1

# Shift screen left
keymap <% SHIFT_LEFT

# Shift screen right
keymap >% SHIFT_RIGHT

# Shift screen one column left
keymap << LEFT

# Shift screen one column right
keymap >> RIGHT

# Vim motions
keymap w NEXT_WORD
keymap W NEXT_WORD
keymap b PREV_WORD
keymap B PREV_WORD
keymap ^ LINE_BEGIN
keymap $ LINE_END
keymap gg BEGIN
keymap G END

# Move by links
keymap DOWN NEXT_LINK
keymap UP PREV_LINK
keymap g0 LINK_BEGIN
keymap g$ LINK_END

# Popup link list menu and move cursor to selected link
keymap ESC-m MOVE_LIST_MENU

# Move to next right (or downward) link
keymap ] NEXT_RIGHT_DOWN

# Move to next left (or upward) link
keymap [ NEXT_LEFT_UP

# Marks
keymap m  MARK
keymap .  NEXT_MARK
keymap ,  PREV_MARK

# Mark all search terms
keymap \"   REG_MARK

# Mark URL-like strings as anchors
keymap :    MARK_URL

# Mark current word as URL
keymap ";"  MARK_WORD
keymap M-:  MARK_MID

# Undo last movement/jump across the page
keymap (    UNDO
keymap )    REDO

# -----------------------------
# Hyperlink Operations
# -----------------------------

# Follow link under cursor
keymap L GOTO_LINK

# View image
keymap u VIEW_IMAGE

# Save image to file
keymap :W SAVE_IMAGE

# Peek current URL
keymap p PEEK

# Peek link URL
keymap P PEEK_LINK

# Peek image URL
keymap U PEEK_IMG

# View info of current document
keymap :i INFO

# Open in external browser
keymap M EXTERN

# Open link in external browser
keymap ESC-M EXTERN_LINK

# Show all links and images
keymap :L LIST

# Popup link list menu and go to selected link
keymap ESC-l LIST_MENU

# Popup acceskey menu
keymap ESC-a ACCESSKEY

# -----------------------------
# File/Stream Operations
# -----------------------------

# Go to URL
keymap :o GOTO
keymap gh GOTO https://duckduckgo.com/lite/

# Go to relative URL
keymap :O GOTO_RELATIVE

# Open URL on new tab
keymap t TAB_GOTO

# Open relative URL on new tab
keymap C-t TAB_GOTO_RELATIVE

# Load local file
keymap :r LOAD

# Execute shell command and load
keymap !r READ_SHELL

# Execute shell command and browse
keymap !| PIPE_SHELL

# Send rendered document to pipe
keymap | PIPE_BUF

# -----------------------------
# Buffer Operations
# -----------------------------

# Move to buffer
keymap I NEXT
keymap E PREV

# Popup buffer selection menu
keymap ESC-b SELECT_MENU

# Go to buffer selection panel
keymap :ls SELECT

# View HTML source
keymap :v SOURCE

# Save document source to file
keymap :w SAVE

# Save buffer to file
keymap :p PRINT

# Edit current document
keymap :e EDIT

# Edit currently rendered document
keymap :E EDIT_SCREEN

# Reload buffer
keymap r RELOAD

# Re-render buffer
keymap R RESHAPE

# Redraw screen
keymap C-l REDRAW

# Restart loading and drawing of images
keymap ESC-u DISPLAY_IMAGE

# Stop loading and drawing of images
keymap X STOP_IMAGE

# -----------------------------
# Tab Operations
# -----------------------------

keymap :t NEW_TAB

keymap x CLOSE_TAB

keymap gt NEXT_TAB
keymap O NEXT_TAB

keymap gT PREV_TAB
keymap N PREV_TAB

# Popup tab selection menu
keymap ESC-t TAB_MENU

# Move current tab 
keymap C-wi TAB_RIGHT
keymap C-wh TAB_LEFT

# -----------------------------
# Searches
# -----------------------------

# Search forward
keymap ESC-/ WHEREIS

# Search backward
keymap ESC-? SEARCH_BACK

# Search next regexp
keymap k SEARCH_NEXT

# Search previous regexp
keymap K SEARCH_PREV

# Incremental search forward
keymap / ISEARCH

# Incremental search backward
keymap ? ISEARCH_BACK

# -----------------------------
# Miscellaneous
# -----------------------------

# Popup menu
keymap SPC MENU

# View help
keymap :h HELP

# yank url under cursor
keymap yy EXTERN_LINK "printf %s "$0" | xsel -b"

# yank url to current page
keymap Y EXTERN "printf %s "$0" | xsel -b"

# Option setting panel
keymap ESC-o OPTIONS

# Display version of w3m
keymap :V VERSION

# View cookie list
keymap :c COOKIE

# View history of URL
keymap :H HISTORY

# Display download list panel
keymap :d DOWNLOAD_LIST

# Change the current document charset
keymap ESC-c CHARSET

# Change the default document charset
keymap ESC-C DEFAULT_CHARSET

# Display error messages
keymap ESC-M MSGS

# Toggle wrap search mode
keymap C-r WRAP_TOGGLE

# Execute w3m command(s)
keymap :: COMMAND

# Set alarm
keymap ESC-A ALARM

# Set option
keymap ESC-O SET_OPTION

# Set environment variable
keymap ESC-E SETENV

# Reload configuration files
keymap :R REINIT

# Execute shell command
keymap :! SHELL

# Stop loading document
keymap C-c SUSPEND

# Quit w3m
keymap :q QUIT

# Quit w3m without confirmation
keymap :x EXIT
keymap ZZ EXIT
