
# video quality 
ytdl_pref="1080+bestaudio/best"

# play videos at 2x speed
url_handler_opts="--speed=2"

# optimize
pages_to_scrape=1
max_thread_count=8
thumbnail_quality="sddefault"

# fanciness
fancy_subs=1
