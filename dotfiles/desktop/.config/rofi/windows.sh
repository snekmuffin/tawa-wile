#/!usr/bin/bash

rofi \
    -modi "window" \
    -show window \
    -theme ~/.config/rofi/theme.rasi
