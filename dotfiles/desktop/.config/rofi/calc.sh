#!/usr/bin/env bash

rofi \
    -modi calc \
    -show calc \
    -theme ~/.config/rofi/theme.rasi
