#/!usr/bin/bash

rofi \
    -modi "drun,run" \
    -show drun \
    -theme ~/.config/rofi/theme.rasi
