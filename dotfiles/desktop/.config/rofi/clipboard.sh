#/!usr/bin/bash

rofi \
    -modi "clipboard:greenclip print" \
    -show clipboard \
    -theme ~/.config/rofi/theme.rasi
