#!/bin/bash
filename="$1"

echo "Number of lines:"
wc -l "$filename"
echo "################"

echo "First 5 lines:"
head -n 5 "$filename"
echo "################"

echo "Last 5 lines:"
tail -n 5 "$filename"
echo "################"
