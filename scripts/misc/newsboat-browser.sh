#!/bin/bash
# opens links that match a query in a modern browser
# otherwise, use the good ol'

websites=("youtube")
do_thing=false

for site in "${websites[@]}"; do
	if [[ "$*" == *"$site"* ]]; then
		do_thing=true
		break
	fi
done

if [[ $do_thing == "true" ]]; then
	waterfox-g "$@"
else
	w3m "$@"
fi
