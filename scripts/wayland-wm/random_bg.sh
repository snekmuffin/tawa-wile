#!/bin/env bash
set -euo pipefail

BG_DIR="$HOME/.pretty/wallpapers/"

walp=$(ls $BG_DIR | sort -R | head -n1)
swaybg -m fill -i "$BG_DIR/$walp" 
