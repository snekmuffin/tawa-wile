#!/bin/sh

# hide terminal cursor, very handy
tput civis

while true; do # prints the calendar, which is configured in ~/.config/khal/config
	khal calendar &
	sleep 15m &&
		clear
done
