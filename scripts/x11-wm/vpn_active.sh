#!/bin/bash

# Checks for status of programs, and displays a string
# to signify which are running

warp_status=$(systemctl is-active warp-plus.service)
nekoray_status=$(pgrep nekoray)
hiddify_status=$(pgrep hiddify)
total_string=""

if [[ -n ${nekoray_status} ]]; then
  total_string="Nekoray, $total_string"
fi

if [[ -n ${hiddify_status} ]]; then
  total_string="Hiddify, $total_string"
fi

if [[ ${warp_status} == "active" ]]; then
  total_string="Warp, $total_string"
fi

if [[ -n ${total_string} ]]; then
  echo ${total_string%,*}  # strip the last comma
else
  echo "None"
fi
