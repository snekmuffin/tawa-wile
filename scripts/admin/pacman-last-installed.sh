#!/bin/bash

for i in $(pacman -Qq)
do
  grep "\[ALPM\] installed $i" /var/log/pacman.log
done | \
  sort -ru | \
  sed -e 's/\[ALPM\] installed //' -e 's/(.*$//' | \
  awk '{print $2}' | bat
