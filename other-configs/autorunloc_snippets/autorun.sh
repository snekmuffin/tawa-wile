run() {
	if ! pgrep -f "$1"; then
		$@ &
	fi
}
run "feishin"
run "obsidian"
run "/usr/bin/kdeconnectd"
run "kdeconnect-indicator"
run "qbittorrent"
run "safeeyes"

# fcitx - japanese input
run fcitx5 -d

# start warp
# warp-cli connect

# to give warp time to connect
# kalu fails to check for updates if done too quickly
sleep 30 && run "kalu"
